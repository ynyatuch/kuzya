
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport core gui
TEMPLATE = app

FORMS += kuzya.ui \
    gotolinedialog.ui \
    optionsdialog.ui \
    finddialog.ui \
    replacedialog.ui \
    aboutdialog.ui

HEADERS += kuzya.h \
    gotolinedialog.h \
    compiler.h \
    optionsdialog.h \
    finddialog.h \
    replacedialog.h \
    helpbrowser.h \
    translator.h \
    version.h \
    aboutdialog.h \
    settingsmanager.h \
    settings.h \
    settingsdefs.h \
    combobox.h

SOURCES += kuzya.cpp \
    main.cpp \
    gotolinedialog.cpp \
    compiler.cpp \
    optionsdialog.cpp \
    finddialog.cpp \
    replacedialog.cpp \
    translator.cpp \
    aboutdialog.cpp \
    settingsmanager.cpp \
    settings.cpp \
    combobox.cpp

RESOURCES = images.qrc

TRANSLATIONS = ../resources/translations/Ukrainian.ts \
    ../resources/translations/English.ts \
    ../resources/translations/Byelorussian.ts \
    ../resources/translations/French.ts \
    ../resources/translations/Serbian.ts \
    ../resources/translations/Arabic.ts

LIBS += -lqscintilla2

unix {

    TARGET = ../bin/kuzya
    target.path = /usr/bin
    translations.path = /usr/share/kuzya/translations
    translations.files = ../resources/translations/*.qm
    kuzya_profiles.path = /usr/share/kuzya/profiles
    kuzya_profiles.files = ../profiles/*
    kuzya_doc.path = /usr/share/kuzya/doc
    kuzya_doc.files = ../doc/Kuzya_Help/*
    kuzya_resources.path = /usr/share/kuzya/resources
    kuzya_resources.files = ../resources/*
    kuzya_icon.path = /usr/share/icons
    kuzya_icon.files = ../resources/icon/*
    kuzya_app_desktop.path = /usr/share/applications
    kuzya_app_desktop.files = ../resources/linux/*

    INSTALLS += target \
        translations \
        kuzya_profiles \
        kuzya_doc \
        kuzya_resources\
    kuzya_icon\
    kuzya_app_desktop
}

win32 {
    TARGET = ../../bin/kuzya
    INCLUDEPATH = ../3rdparty/QScintilla/win32/2.8.2/include
    win32-g++ {
        greaterThan(QT_MAJOR_VERSION, 4): LIBS += -L../3rdparty/QScintilla/win32/2.8.2/lib/mingw/debug
        lessThan(QT_MAJOR_VERSION, 5):LIBS += -L../3rdparty/QScintilla/win32/2.8.2/lib/mingw/debug_old
    } else {
        greaterThan(QT_MAJOR_VERSION, 4):LIBS += -L../3rdparty/QScintilla/win32/2.8.2/lib/msvc2010/debug
        lessThan(QT_MAJOR_VERSION, 4):LIBS += -L../3rdparty/QScintilla/win32/2.8.2/lib/msvc2010/debug_old
    }
}

mac {
    CONFIG-=app_bundle
    DESTDIR +=../kuzya
    TARGET +=kuzya
}
