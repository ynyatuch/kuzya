#include "settingsmanager.h"
#include "settings.h"
#include <QMessageBox>
#include <QString>
#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#endif


#include <QDir>
#include <qdebug.h>
SettingsManager::SettingsManager(QObject *parent) :
    QObject(parent)
{
    QString lSettingsDirPath;
   // QString lSettingsDirPath = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#if QT_VERSION >= 0x050000
    lSettingsDirPath = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
#else
    lSettingsDirPath = QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif

    QDir().mkpath(lSettingsDirPath);
    QString lSettingsPath = QString("%1/settings.ini")
        .arg(lSettingsDirPath);
    mSettings = new QSettings(lSettingsPath, QSettings::IniFormat);
}

void SettingsManager::setValue(const QString &pGroups, const QString &pKey, const QVariant &pValue)
{
    mSettings->beginGroup(pGroups);
    mSettings->setValue(pKey,pValue);
    mSettings->endGroup();

}

QVariant SettingsManager::value(const QString &pGroups, const QString &pKey,const QVariant &pValue)
{
    mSettings->beginGroup(pGroups);
    QVariant lValue =  mSettings->value(pKey,pValue);
    mSettings->endGroup();
    return lValue;
}

int SettingsManager::beginReadArray(const QString &pArrayName)
{
    int size = mSettings->beginReadArray(pArrayName);
    return size;
}

void SettingsManager::setArrayIndex(const int &i)
{
    mSettings->setArrayIndex(i);
}

void SettingsManager::sync()
{
    mSettings->sync();
}

void SettingsManager::endArray()
{
    mSettings->endArray();
}

void SettingsManager::remove(const QString &pKey)
{
    mSettings->remove(pKey);
}

void SettingsManager::beginWriteArray(const QString &pArrayName)
{
    mSettings->beginWriteArray(pArrayName);
}

void SettingsManager::clear()
{
    mSettings->clear();
}

void SettingsManager::slotSettingsChanged(const QMap<QString, QVariant> lSettingsMap)
{
    QMap<QString, QVariant>::const_iterator it;
    for (it = lSettingsMap.begin();
         it != lSettingsMap.end();
         it++)
    {
        setValue("Settings",it.key(), it.value());
    }
}




