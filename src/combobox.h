#ifndef COMBOBOX_H
#define COMBOBOX_H

#include <QComboBox>

class ComboBox : public QComboBox
{
    Q_OBJECT
    Q_PROPERTY(QString changedText READ changedText WRITE setChangedText NOTIFY changedTextChanged)
    Q_PROPERTY(QString editedText READ editedText WRITE setEditedText NOTIFY editedTextChanged)
public:
    explicit ComboBox(QWidget *parent = 0);

    QString changedText() const;
    QString editedText() const;
signals:
    void changedTextChanged(QString);
    void editedTextChanged(QString);
public slots:
    void setChangedText(const QString &text);
    void setEditedText(const QString &text);
private:
    QString mChangedText;
    QString mEditedText;
};

#endif // COMBOBOX_H
