#include "combobox.h"
#include <QDebug>
ComboBox::ComboBox(QWidget *parent) :
    QComboBox(parent)
{
    connect(this, SIGNAL(activated(QString)), this, SLOT(setChangedText(QString)), Qt::UniqueConnection);
    connect(this, SIGNAL(editTextChanged(QString)), this, SLOT(setEditedText(QString)), Qt::UniqueConnection);
}

QString ComboBox::changedText() const
{
    return mChangedText;
}

QString ComboBox::editedText() const
{
    return mEditedText;
}

void ComboBox::setChangedText(const QString &text)
{
    if (mChangedText != text)
    {
        mChangedText = text;
        emit changedTextChanged(mChangedText);
    }
}

void ComboBox::setEditedText(const QString &text)
{
    if (mEditedText != text)
    {
        mEditedText = text;
        emit editedTextChanged(mEditedText);
    }
}
