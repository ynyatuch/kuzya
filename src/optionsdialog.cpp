/******************************************************************************
 *   Copyright (C) 2008 by                                                    *
 *                     Volodymyr Shevchyk (volder@users.sourceforge.net),     *
 *                     Victor Sklyar (bouyantgrambler@users.sourceforge.net), *
 *                     Alex Chmykhalo (alexchmykhalo@users.sourceforge.net)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>     *
 ******************************************************************************/

#include <Qsci/qsciscintilla.h>
#include <QListWidget>
#include <QMessageBox>
#include <QFontDialog>
#include <QColorDialog>
#include <QColor>
#include <QFileDialog>
#include <QDebug>
#include <QLocale>
#include "optionsdialog.h"
#include "settingsmanager.h"
#include "settings.h"
#include "settingsdefs.h"

OptionsDialog::OptionsDialog(QWidget *parent)
    : QDialog(parent),
      mSettings(0)
{
    setupUi(this);
    mw =(Kuzya*)parent;
    setWindowTitle(tr("Settings"));
    mSettingsManager = new SettingsManager();
    readODWSettings();
    connect(closeBtn,SIGNAL(clicked()), this,SLOT(close()));
    connect(okBtn,	 SIGNAL(clicked()), this,SLOT(close()));
    connect(defaultBtn,SIGNAL(clicked()),this,SLOT(slotDefaultAll(void)));
    connect(directoryBox,SIGNAL(activated(int)),this,SLOT(slotChangeDefDir(int)));
    connect(directoryBox,SIGNAL(editTextChanged(QString)),this,SLOT(slotChangeDefDir(QString)));
    connect(languageComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(slotUpdateCompilerCBox(QString)));
    connect(compilerComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(slotLoadCompilerOptions(QString)));
    connect(browseButton, SIGNAL(clicked()), this, SLOT(slotChangeCompilerLocation()));
    connect(defaultUsePushButton, SIGNAL(clicked()), this, SLOT(slotDefaultCompiler()));
    connect(compilerResetPushButton, SIGNAL(clicked()) ,this, SLOT(slotResetCompilerOptions()));
    connect(mTabIconView,SIGNAL(currentRowChanged(int)),this,SLOT(slotChangeOptionPage(int)));
    ///-----------------------------Fonts and Colors-------------------------------------------------------
    styleCBox->addItems(QStyleFactory::keys());
#ifdef WIN32
    stylesDir=QDir(QApplication::applicationDirPath()+"/../resources/qss/");
    localizationLanguageDir=QDir(QApplication::applicationDirPath()+"/../resources/translations/");
#else
    stylesDir=QDir(QDir::cleanPath(QApplication::applicationDirPath() + "/../../usr/share/kuzya/resources/qss/"));
    if (stylesDir.exists() == false)
    {
        stylesDir=QDir(QDir::cleanPath(QApplication::applicationDirPath() + "/../"+STYLESHEETS_RELATIVE_PATH));
        if (stylesDir.exists() == false)
        {
            QMessageBox msgBox;
            msgBox.setText("There is some problem with loading of styles");
            msgBox.exec();
        }
    }
    localizationLanguageDir=QDir(QDir::cleanPath(QApplication::applicationDirPath() + "/../../usr/share/kuzya/resources/translations/"));
    if (localizationLanguageDir.exists() == false)
    {
        localizationLanguageDir=QDir(QDir::cleanPath(QApplication::applicationDirPath() + "/../"+TRANSLATIONS_REALATIVE_PATH));
        if (localizationLanguageDir.exists() == false)
        {
            QMessageBox msgBox;
            msgBox.setText("There is some problem with loading of translations");
            msgBox.exec();
        }
    }
#endif
    languageComboBox->clear();
    QStringList supportedList = mw->getCurrentCompiler()->getSupportedLanguages();
    supportedList.sort();
    languageComboBox->addItems(supportedList);
    styleFilters<<"*.qss";
    localizationLanguageFilters<<"*.qm";
    stylesDir.setNameFilters(styleFilters);
    localizationLanguageDir.setNameFilters(localizationLanguageFilters);
    slotUpdateSkinsCBox();
    slotUpdatelocalizationLanguageCBox();
}
void OptionsDialog::slotUpdateSkinsCBox(void)
{
    QStringList skinsNameList = stylesDir.entryList(stylesDir.nameFilters(),QDir::Files,QDir::Name);
    for(int i = 0;i<skinsNameList.count();i++)
    {
        skinsNameList[i]=skinsNameList.at(i).left(skinsNameList.at(i).lastIndexOf("."));
    }
    skinCBox->addItems(skinsNameList);
}
void OptionsDialog::slotUpdatelocalizationLanguageCBox()
{
    QStringList localizationLanguageList = localizationLanguageDir.entryList(localizationLanguageDir.nameFilters(),QDir::Files,QDir::Name);
    for(int i=0;i<localizationLanguageList.count();i++)
    {
        localizationLanguageList[i]=localizationLanguageList.at(i).left(localizationLanguageList.at(i).lastIndexOf("."));
        localizationLanguageCBox->addItem(QIcon(":flags/images/flags/"+localizationLanguageList[i]+".png"),localizationLanguageList[i]);
    }
}

OptionsDialog::~OptionsDialog()
{
}
void OptionsDialog::retranslate(void)
{
    retranslateUi(this);
}
///*******writeSettings******************************************************************************
void OptionsDialog::writeSettings(void)
{
    ///-----PROGRAMING--LANGUAGE---------------------------------------------------
    QString val = languageComboBox->currentText()+"/"+compilerComboBox->currentText();
    QString location = compilerDirLineEdit->text().replace("/", QDir::separator());
    if (!location.isEmpty())
    {
        compilerDirLineEdit->setText(location);
    }
    mSettingsManager->setValue("compilation_settings",val+"/location", location);
    mSettingsManager->setValue("compilation_settings",val+"/options", compilerOptionsEdit->toPlainText().remove("\n"));/**/
    mSettingsManager->sync();
}

///******readOptionDialogWindowSettings********************************************************************

void OptionsDialog::readODWSettings()
{
    checkBox->setChecked(mSettingsManager->value("Settings",OPEN_LAST_PROJECT,false).toBool());
    ///-----------------------------------------------------------------------------
    sB_LOFCount->setValue(mSettingsManager->value("Settings",LAST_OPENED_FILES_NUMBER,5).toInt());
    ///-----Style&Skins----------------------------------------------------------------------
    styleCBox->setCurrentIndex(styleCBox->findText(mSettingsManager->value("Settings",APPLICATION_STYLE, "Cleanlooks").toString()));
    skinCBox->setCurrentIndex(skinCBox->findText(mSettingsManager->value("Settings",APPLICATION_SKIN,"creation").toString()));
    ///-----APPLICATION_LANGUAGE-------------------------------------------------------------------------
    localizationLanguageCBox->setCurrentIndex(localizationLanguageCBox->findText(mSettingsManager->value("Settings",APPLICATION_LANGUAGE,"English.qm").toString()));
    ///-----DefaultDirectory-------------------------------------------------------------------------
    directoryBox->setCurrentIndex(0);
    if(directoryBox->findText(mSettingsManager->value("Settings", DEFAULT_DIRECTORY,QDir::homePath()).toString())==-1)
    {
        directoryBox->insertItem(0,mSettingsManager->value("Settings",DEFAULT_DIRECTORY,QDir::homePath()).toString());
        directoryBox->removeItem(1);
        directoryBox->setCurrentIndex(0);
    }
    ///-----PROGRAMING--LANGUAGE---------------------------------------------------
    slotLoadCompilerOptions(compilerComboBox->currentText());
    ///***************************TEXT_EDITOR************************`**************************************
    lineNumbCHB->setChecked(mSettingsManager->value("/Settings/TextEditor","ShowLineNumber",false).toBool());
    autoComplCHB->setChecked(mSettingsManager->value("Settings",USE_AUTO_COMPLETION,false).toBool());
    wordWrapCHB->setChecked(mSettingsManager->value("Settings",DYNAMIC_WORD_WRAP,false).toBool());
    foldingMarkersCHB->setChecked(mSettingsManager->value("Settings", SHOW_FOLDING_MARKERS,false).toBool());
    autoIndentCHB->setChecked(mSettingsManager->value("Settings",AUTOMATIC_INDETATION,false).toBool());
    indentLineCHB->setChecked(mSettingsManager->value("Settings",SHOW_INDETATION_LINES,false).toBool());
    tabKeyindentCHB->setChecked(mSettingsManager->value("Settings",TAB_KEY_INDENTS,true).toBool());
    tabWidthSpinBox->setValue(mSettingsManager->value("Settings",TAB_KEY_WIDTH,4).toInt());
}

////*******slotDefaultAll**********************************************************************************

void OptionsDialog::slotDefaultAll(void)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Set default settings?"));
    msgBox.setText(tr("Are you sure?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    msgBox.exec();

    if (msgBox.clickedButton() != msgBox.defaultButton())
    {
        mSettingsManager->remove(RECENT_FILES);
        mSettingsManager->clear();
        readODWSettings();
        setDefaultAll(true);
    }
}

void OptionsDialog::slotChangeDefDir(int index)
{
    if(directoryBox->itemText(index)==tr("<Other...>"))
    {
        QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                        "/home",
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);
        if((dir!="")&&(directoryBox->findText(dir)==-1))
        {
            directoryBox->removeItem(0);
            directoryBox->insertItem(0,dir);
        }
        directoryBox->setCurrentIndex(0);
    }
}

void OptionsDialog::slotChangeDefDir(QString dirName)
{
    if (QDir(dirName).exists())
    {
        errorInformLabel->setStyleSheet("");
        errorInformLabel->setText("");
        applyBtn->setEnabled(true);
        okBtn->setEnabled(true);
    }
    else
    {
        errorInformLabel->setText(tr("The path:")+dirName+tr(" not exists"));
        errorInformLabel->setStyleSheet("background-color:red;");
        applyBtn->setEnabled(false);
        okBtn->setEnabled(false);
    }
}

void OptionsDialog::slotUpdateCompilerCBox(QString lang)
{
    QStringList compilers = mw->getCurrentCompiler()->getSupportedCompilers(lang);
    compilers.sort();
    compilerComboBox->clear();
    compilerComboBox->addItems(compilers);
    QString defaultComp = readDefaultCompiler(lang);
    int index = compilerComboBox->findText(defaultComp);
    if (-1 != index) compilerComboBox->setCurrentIndex(index);
}

void OptionsDialog::slotLoadCompilerOptions(QString comp)
{
    QString lang = languageComboBox->currentText();
    QString info = mw->getCurrentCompiler()->getCompilerInfo(lang, comp);
    compilerInfo->setText(info);
    QString val = languageComboBox->currentText()+"/"+compilerComboBox->currentText();
    compilerDirLineEdit->setText(mSettingsManager->value("compilation_settings",val+"/location","").toString());
    compilerOptionsEdit->setPlainText(mSettingsManager->value("compilation_settings",val+"/options","").toString());
}

void OptionsDialog::slotChangeCompilerLocation()
{
    QString comp = compilerComboBox->currentText();
    QString dir = QFileDialog::getExistingDirectory(this,
                                                    tr("Show compiler location: (")+comp+")",
                                                    "",
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if((""!=dir)&&(directoryBox->findText(dir)==-1))
    {
        compilerDirLineEdit->clear();
        compilerDirLineEdit->setText(dir.replace("/", QDir::separator()));
    }
}

QString OptionsDialog::readDefaultCompiler(QString lang)
{
    QString def = mSettingsManager->value("compilation_settings",lang+"/default","").toString();
    return def;
}

QString OptionsDialog::readCompilerLocation(QString lang, QString comp)
{
    QString location = mSettingsManager->value("compilation_settings",lang+"/"+comp+"/location","").toString();
    return location;
}

QString OptionsDialog::readCompilerOptions(QString lang, QString comp)
{
    QString options = mSettingsManager->value("compilation_settings",lang+"/"+comp+"/options","").toString();
    return options;
}

bool OptionsDialog::defaultAll() const
{
    return mDefaultAll;
}


void OptionsDialog::setSettings(Settings *settings)
{
    mSettings = settings;
    if (mSettings)
    {
        mSettings->add(SHOW_LINE_NUMBER_SETTING, this->lineNumbCHB, "checked");
        mSettings->add(USE_AUTO_COMPLETION,this->autoComplCHB,"checked");
        mSettings->add(DYNAMIC_WORD_WRAP,this->wordWrapCHB,"checked");
        mSettings->add(SHOW_FOLDING_MARKERS,this->foldingMarkersCHB,"checked");
        mSettings->add(AUTOMATIC_INDETATION,this->autoIndentCHB,"checked");
        mSettings->add(SHOW_INDETATION_LINES,this->indentLineCHB,"checked");
        mSettings->add(BACKSPACE_KEY_INDENTS,this->BkspaceIndentCHB,"checked");
        mSettings->add(TAB_KEY_INDENTS,this->tabKeyindentCHB,"checked");
        mSettings->add(TAB_KEY_WIDTH,this->tabWidthSpinBox,"value");
        mSettings->add(OPEN_LAST_PROJECT,this->checkBox,"checked");
        mSettings->add(LAST_OPENED_FILES_NUMBER,this->sB_LOFCount,"value");
        mSettings->add(APPLICATION_STYLE,this->styleCBox,"changedText");
        mSettings->add(APPLICATION_SKIN,this->skinCBox,"changedText");
        mSettings->add(APPLICATION_LANGUAGE,this->localizationLanguageCBox,"changedText");
        mSettings->add(DEFAULT_ALL,this,"defaultAll");
        connect(closeBtn,SIGNAL(clicked()), mSettings,SLOT(revert()));
        connect(applyBtn,SIGNAL(clicked()), mSettings,SLOT(commit()));
        connect(okBtn,SIGNAL(clicked()), mSettings,SLOT(commit()));
    }
}

void OptionsDialog::slotDefaultCompiler()
{
    QString lang = languageComboBox->currentText();
    mSettingsManager->setValue("compilation_settings",lang+"/default",compilerComboBox->currentText());
}

void OptionsDialog::slotResetCompilerOptions()
{
    QString lang = languageComboBox->currentText();
    QString comp = compilerComboBox->currentText();
    QString location = mSettingsManager->value("default_compilation_settings",lang+"/"+comp+"/location","").toString();
    QString options = mSettingsManager->value("default_compilation_settings",lang+"/"+comp+"/options","").toString();
    compilerDirLineEdit->setText(location);
    compilerOptionsEdit->setPlainText(options);
}

void OptionsDialog::slotChangeOptionPage(int pIndex)
{
    mStackedWidget->setCurrentIndex(pIndex);
}

void OptionsDialog::setDefaultAll(const bool text)
{
    if (mDefaultAll != text)
    {
        mDefaultAll = text;
        emit defaultAllChanged(mDefaultAll);
    }
}

void OptionsDialog::slotCompilationSettingsChange(QString)
{

}
