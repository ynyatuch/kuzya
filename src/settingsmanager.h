#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QObject>
#include <QSettings>
class Settings;
class SettingsManager : public QObject
{
    Q_OBJECT
public:
    explicit SettingsManager(QObject *parent = 0);
    void setValue(const QString &pGroups, const QString &pKey, const QVariant &pValue);
    QVariant value(const QString &pGroups, const QString &pKey, const QVariant &pValue);
    int beginReadArray(const QString &pArrayName);
    void setArrayIndex(const int &i);
    void sync();
    void endArray();
    void remove(const QString &pKey);
    void beginWriteArray(const QString &pArrayName);
    void clear();
public slots:
    void slotSettingsChanged(const QMap<QString, QVariant> lSettingsMap);
private:
    QSettings *mSettings;
    Settings *mSettingsChange;
};

#endif // SETTINGSMANAGER_H
