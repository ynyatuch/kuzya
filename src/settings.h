#ifndef PLUGINSETTINGS_H
#define PLUGINSETTINGS_H

#include <QObject>
#include <QMultiMap>
#include <QVariant>
#include <QMetaProperty>
#include <QDebug>

#include <QSignalMapper>

class SettingsManager;
class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = 0);

    QString settingsPath() const;
    void setSettingsPath(const QString& pSettingsPath);
    bool add(const QString& pName, QObject *pObject,
            const QString& pProperty);
    bool subscribe(const QString& pName, QObject *pObject,
            const QByteArray &pSignature);
    bool isPropertyCouldBeAttached(QObject *pObject,
            const QString& pProperty);
    bool isMethodCouldBeSubscribed(QObject *pObject,
            const QString &pSignature);
    SettingsManager* mSettingsManager;
signals:
    void settingsChanged(QMap<QString, QVariant>);

public slots:
    void commit();
    void revert();
    void slotSetSettings(QMap<QString, QVariant> pMap);

private:
    QVariant value(const QString &pSetting) const;
    void setValue(const QString &pSetting, const QVariant &pValue);
    void notifySubscribers(const QString &pName, const QVariant &pValue);
    QMetaProperty metaProperty(QObject *pObject, const QString& pProperty);
    QMetaMethod metaMethod(QObject *pObject, const QByteArray &pSignature);
    void invoke(QObject *pObject, const QByteArray& pSignature, const QVariant &pValue);

private:
    QString mSettingsPath;
    QMap<QString, QObject*> mObjectBySetting;
    QMap<QString, QByteArray> mPropertyBySetting;
    QMap<QString, QObject*> mSubscribedObjectsBySetting;
    QMap<QString, QByteArray> mSubscribedMethodBySetting;
    QMap<QString, QVariant> mSettingValueByName;
    QMap<QString, QVariant> mModifiedSettingsByName;
    QSignalMapper *mPropertyMapper;

private slots:
    void propertyChanged(QString pName);

};

#endif // PLUGINSETTINGS_H
