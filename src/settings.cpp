#include "settings.h"
#include "settingsmanager.h"
Settings::Settings(QObject *parent) :
    QObject(parent),
    mPropertyMapper(new QSignalMapper(this))
{
    mSettingsManager = new SettingsManager();
    connect(mPropertyMapper, SIGNAL(mapped(QString)),
            this, SLOT(propertyChanged(QString)), Qt::UniqueConnection);
}

QString Settings::settingsPath() const
{
    return mSettingsPath;
}

bool Settings::add(const QString &pName, QObject *pObject, const QString &pProperty)
{
    bool lSuccess = false;
    if (pName.isEmpty() || !pObject || pProperty.isEmpty())
    {
        qDebug("Settings: could not add setting - invalid parameter value");
    }
    if ((lSuccess = isPropertyCouldBeAttached(pObject, pProperty)))
    {
        mObjectBySetting.insert(pName, pObject);
        mPropertyBySetting.insert(pName, pProperty.toUtf8());
        mPropertyMapper->setMapping(pObject, pName);
        QMetaProperty lMetaProperty = metaProperty(pObject, pProperty);
        QMetaMethod lPropertySignal = lMetaProperty.notifySignal();
        QMetaMethod lMapMethod = metaMethod(mPropertyMapper, "map()");
        connect(pObject, lPropertySignal, mPropertyMapper, lMapMethod, Qt::UniqueConnection);
        if (value(pName)=="")
        {
            mSettingValueByName.insert(pName,mSettingsManager->value("Settings",pName,""));
        }
        else
        {
            mSettingValueByName.insert(pName, value(pName));
        }
    }
    return lSuccess;
}

bool Settings::subscribe(const QString &pName, QObject *pObject, const QByteArray &pSignature)
{

    bool lResult = false;
    if (!pObject || pSignature.isEmpty())
    {
        qDebug("Settings: could not subscribe - invalid parameter value");
    }
    if ((lResult = isMethodCouldBeSubscribed(pObject, pSignature)))
    {
        mSubscribedObjectsBySetting.insert(pName, pObject);
        mSubscribedMethodBySetting.insert(pName, pSignature);
    }
    return lResult;

}

void Settings::commit()
{
    QMap<QString, QVariant>::const_iterator it;
    for (it = mModifiedSettingsByName.begin();
         it != mModifiedSettingsByName.end();
         it++)
    {
        mSettingValueByName.insert(it.key(), it.value());
    }
    emit settingsChanged(mModifiedSettingsByName);
    mModifiedSettingsByName.clear();
}

void Settings::revert()
{
    foreach (QString lName, mModifiedSettingsByName.keys())
    {
        QVariant oldValue = mSettingValueByName.value(lName);
        setValue(lName, oldValue);
    }
    mModifiedSettingsByName.clear();
}

bool Settings::isPropertyCouldBeAttached(QObject *pObject, const QString &pProperty)
{
    bool lResult = false;
    QMetaProperty lMetaProperty = metaProperty(pObject, pProperty);
    if (lMetaProperty.isValid())
    {
        lResult = lMetaProperty.isValid()
            && lMetaProperty.isWritable()
            && lMetaProperty.isReadable()
            && lMetaProperty.hasNotifySignal();
    }
    return lResult;
}

bool Settings::isMethodCouldBeSubscribed(QObject *pObject, const QString &pSignature)
{
    //.........
    return true;
}

void Settings::slotSetSettings(QMap<QString, QVariant> pMap)
{
    QMap<QString, QVariant>::const_iterator it;
    for (it = pMap.begin();
         it != pMap.end();
         it++)
    {
        mSettingValueByName.insert(it.key(), it.value());
        setValue(it.key(), it.value());
        notifySubscribers(it.key(), it.value());
    }
}

void Settings::setSettingsPath(const QString &pSettingsPath)
{
    mSettingsPath = pSettingsPath;
}

QVariant Settings::value(const QString &pSetting) const
{
    QVariant lResult;
    QObject *pObject = mObjectBySetting.value(pSetting);
    if (pObject && mPropertyBySetting.contains(pSetting))
    {
        lResult = pObject->property(mPropertyBySetting[pSetting].constData());
    }
    return lResult;
}

void Settings::setValue(const QString &pSetting, const QVariant &pValue)
{
    QObject *pObject = mObjectBySetting.value(pSetting);
    QByteArray pProperty = mPropertyBySetting.value(pSetting);
    if (pObject && !pProperty.isEmpty())
    {
        pObject->setProperty(pProperty.constData(), pValue);
    }
}

void Settings::notifySubscribers(const QString &pName, const QVariant &pValue)
{
    QObject *lObject = mSubscribedObjectsBySetting.value(pName);
    QByteArray lSignature = mSubscribedMethodBySetting.value(pName);
    if (lObject && !lSignature.isEmpty())
    {
        invoke(lObject, lSignature, pValue);
    }
}

QMetaProperty Settings::metaProperty(QObject *pObject, const QString &pProperty)
{
    QMetaProperty rMetaProperty;
    if (const QMetaObject *lMetaObject = pObject->metaObject())
    {
        QByteArray data = pProperty.toUtf8();
        int lPropertyIndex = lMetaObject->indexOfProperty(data.constData());
        if (lPropertyIndex >= 0)
        {
            rMetaProperty = lMetaObject->property(lPropertyIndex);
        }
    }
    return rMetaProperty;
}

QMetaMethod Settings::metaMethod(QObject *pObject, const QByteArray &pSignature)
{
    QByteArray lSignature = QMetaObject::normalizedSignature(pSignature.constData());
    int lMethodIndex = pObject->metaObject()->indexOfSlot(lSignature);
    return mPropertyMapper->metaObject()->method(lMethodIndex);
}

void Settings::invoke(QObject *pObject, const QByteArray &pSignature, const QVariant &pValue)
{
    pObject->metaObject()->invokeMethod(pObject, pSignature.constData(),
                                        Qt::QueuedConnection, Q_ARG(QVariant, pValue));
}

void Settings::propertyChanged(QString pName)
{
    QVariant settingValue = value(pName);
    if (mSettingValueByName.value(pName) != settingValue)
    {
        mModifiedSettingsByName.insert(pName, settingValue);
    }
    else
    {
        mModifiedSettingsByName.remove(pName);
    }
    notifySubscribers(pName,settingValue);
}
