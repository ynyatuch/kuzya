#ifndef SETTINGSDEFS_H
#define SETTINGSDEFS_H

#include <QString>



const QString SHOW_LINE_NUMBER_SETTING = "TextEditor/ShowLineNumber";
const QString USE_AUTO_COMPLETION = "TextEditor/WordCompletion";
const QString DYNAMIC_WORD_WRAP = "TextEditor/WordWrap";
const QString SHOW_FOLDING_MARKERS = "TextEditor/CodeFolding";
const QString AUTOMATIC_INDETATION ="TextEditor/Indentation/AutoIndentation";
const QString SHOW_INDETATION_LINES = "TextEditor/Indentation/ShowIndentLine";
const QString BACKSPACE_KEY_INDENTS = "TextEditor/Indentation/UseBackspaceIndent";
const QString TAB_KEY_INDENTS ="TextEditor/Indentation/UseTabIndent";
const QString TAB_KEY_WIDTH ="TextEditor/Indentation/TabIndentWidth";
const QString APPLICATION_STYLE = "MainWindow/Interface/Style";
const QString APPLICATION_SKIN = "MainWindow/Interface/Skin";
const QString OPEN_LAST_PROJECT = "MainWindow/StartupPro";
const QString LAST_PROJECT_NAME = "MainWindow/LastProjectName";
const QString LAST_OPENED_FILES_NUMBER = "MainWindow/LOFCount";
const QString APPLICATION_LANGUAGE = "MainWindow/Language";
const QString RECENT_FILES = "Settings/MainWindow/RecentFiles";
const QString DEFAULT_DIRECTORY = "MainWindow/DefaultDir";
const QString MAIN_WINDOW_STATE = "MainWindow/MainWindowState";
const QString COMPILATION_SETTINGS ="compilation_settings";
const QString PROGRAMMING_LANGUAGE = "programming_language";
const QString DEFAULT_ALL = "default_all";
const QString DEFAULT_COMPILATION_SETTINGS = "default_compilation_settings";

#endif // SETTINGSDEFS_H
