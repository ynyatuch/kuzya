/******************************************************************************
 *   Copyright (C) 2008 by                                                    *
 *                     Volodymyr Shevchyk (volder@users.sourceforge.net),     *
 *                     Victor Sklyar (bouyantgrambler@users.sourceforge.net), *
 *                     Alex Chmykhalo (alexchmykhalo@users.sourceforge.net)   *
 *                                                                            *
 *   This program is free software: you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation, either version 3 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>     *
 ******************************************************************************/

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QObject>
#include <QDialog>
#include <QPalette>
#include <QSettings>
#include <QTranslator>
#include <QTextStream>
#include <QFile>
#include <QDir>
#include <QStyleFactory>
#include "ui_optionsdialog.h"
#include "kuzya.h"

#define STYLESHEETS_RELATIVE_PATH "resources/qss/"
#define TRANSLATIONS_REALATIVE_PATH "resources/translations/"

class QListWidget;
class QsciScintilla;
class Kuzya;
class SettingsManager;
class Settings;

class OptionsDialog : public QDialog, private Ui::optionsForm
{
    Q_OBJECT
    Q_PROPERTY(bool defaultAll READ defaultAll WRITE setDefaultAll NOTIFY defaultAllChanged)

public:
    OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();
    void setSettings(Settings *);
    void writeSettings(void);
    void readODWSettings();
    void retranslate(void);
    bool ukrIsCheked();
    QString readDefaultCompiler(QString lang);
    QString readCompilerLocation(QString lang, QString comp);
    QString readCompilerOptions(QString lang, QString comp);
    QString savedname;
    QDir localizationLanguageDir;
    Settings* mSettings;
    Kuzya* mw;
    bool defaultAll() const;
signals:
    void signalApply();
    void signalGhange(QMap<QString,QVariant>);
    void defaultAllChanged(bool);
    void signalCompilationSettingsChanged(QVariant);
private slots:
    void slotDefaultAll(void);
    void slotChangeDefDir(int);
    void slotChangeDefDir(QString);
    void slotUpdateSkinsCBox(void);
    void slotUpdatelocalizationLanguageCBox();
    void slotUpdateCompilerCBox(QString);
    void slotLoadCompilerOptions(QString);
    void slotChangeCompilerLocation();
    void slotDefaultCompiler();
    void slotResetCompilerOptions();
    void slotChangeOptionPage(int);
    void setDefaultAll(const bool text);
    void slotCompilationSettingsChange(QString);
private:
    QTranslator* trans_ua;
    QTranslator* trans_en;
    QFile file;
    QSettings* settings;
    QTranslator translator;
    QFont font;
    QDir stylesDir;
    QStringList styleFilters;
    QStringList localizationLanguageFilters;
    SettingsManager* mSettingsManager;
    QString mStyleText;
    QString mSkinText;
public:
    bool isLineMarginVisible;
    bool mDefaultAll;
};

#endif
